package com.sjdang.springboot.dubbo.api;

import java.util.List;

import com.sjdang.springboot.dubbo.entity.CompanyEntity;

public interface CompanyService {
	
	List<CompanyEntity> findAll();
	
	CompanyEntity findOne(String companyId);
	
	Integer getCount();

}
