package com.sjdang.springboot.dubbo.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Service;
import com.sjdang.springboot.dubbo.api.CompanyService;
import com.sjdang.springboot.dubbo.entity.CompanyEntity;
import com.sjdang.springboot.dubbo.repository.CompanyRepository;

@Service
@Component
public class CompanyServiceImpl implements CompanyService {

	@Resource
	private CompanyRepository companyRepository;
	
	@Override
	public List<CompanyEntity> findAll() {
		return companyRepository.findAll();
	}

	@Override
	public CompanyEntity findOne(String companyId) {
		return companyRepository.getById(companyId);
	}

	@Override
	public Integer getCount() {
		return (int) companyRepository.count();
	}

}
