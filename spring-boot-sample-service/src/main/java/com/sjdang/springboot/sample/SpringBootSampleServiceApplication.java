package com.sjdang.springboot.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource(value = "classpath:config/dubbo-provider.xml")
public class SpringBootSampleServiceApplication {

    public static void main(String[] args) {
    	
        SpringApplication.run(SpringBootSampleServiceApplication.class, args);
        
    }

}
