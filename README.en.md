# springboot-dubbo-sample

一、介绍

SpringBoot + Dubbo + ZooKeeper + MySQL 示例

二、软件架构
1. SpringBoot
2. Dubbo
3. ZooKeeper
4. MySQL

三、工程代码安装部署

![SpringBoot集成Dubbo](https://images.gitee.com/uploads/images/2021/0817/103426_ac248626_9503254.png "屏幕截图.png")

四、Dubbo Admin安装

![Dubbo Admin微服务管理控制台](https://images.gitee.com/uploads/images/2021/0817/103244_d446b68c_9503254.png "屏幕截图.png")
