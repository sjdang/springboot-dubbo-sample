package com.sjdang.springboot.dubbo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sjdang.springboot.dubbo.entity.CompanyEntity;

public interface CompanyRepository extends JpaRepository<CompanyEntity, String> {

}
