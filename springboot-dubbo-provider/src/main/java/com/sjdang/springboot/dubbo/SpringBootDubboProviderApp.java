package com.sjdang.springboot.dubbo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource(value = "classpath:config/dubbo-provider.xml")
public class SpringBootDubboProviderApp {

    public static void main(String[] args) {
    	
        SpringApplication.run(SpringBootDubboProviderApp.class, args);
        
    }

}
