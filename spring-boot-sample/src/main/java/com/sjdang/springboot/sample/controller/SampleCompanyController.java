package com.sjdang.springboot.sample.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sjdang.springboot.sample.api.CompanyService;
import com.sjdang.springboot.sample.entity.CompanyEntity;

@Controller
@RequestMapping("/sample/company")
public class SampleCompanyController {
	
	@Reference(version = "1.0.0")
	private CompanyService companyService;
	
	@RequestMapping("/view")
	public String view(Model model) {
		List<CompanyEntity> companyList = companyService.findAll();
		model.addAttribute("companies", companyList);
		return "html/springboot-company.html";
	}

}
