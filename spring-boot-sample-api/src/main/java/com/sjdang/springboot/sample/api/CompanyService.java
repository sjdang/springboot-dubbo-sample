package com.sjdang.springboot.sample.api;

import java.util.List;

import com.sjdang.springboot.sample.entity.CompanyEntity;

public interface CompanyService {
	
	List<CompanyEntity> findAll();

}
