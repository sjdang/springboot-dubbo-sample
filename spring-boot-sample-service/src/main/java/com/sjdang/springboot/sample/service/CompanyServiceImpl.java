package com.sjdang.springboot.sample.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Service;
import com.sjdang.springboot.sample.api.CompanyService;
import com.sjdang.springboot.sample.entity.CompanyEntity;
import com.sjdang.springboot.sample.repository.CompanyRepository;

@Service
@Component
public class CompanyServiceImpl implements CompanyService {

	@Resource
	private CompanyRepository companyRepository;
	
	@Override
	public List<CompanyEntity> findAll() {
		return companyRepository.findAll();
	}

}
