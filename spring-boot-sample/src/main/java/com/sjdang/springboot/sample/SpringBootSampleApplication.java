package com.sjdang.springboot.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource(value = "classpath:config/dubbo-consumer.xml")
public class SpringBootSampleApplication {

    public static void main(String[] args) {
    	
        SpringApplication.run(SpringBootSampleApplication.class, args);
        
    }

}
