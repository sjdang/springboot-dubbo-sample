package com.sjdang.springboot.sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sjdang.springboot.sample.entity.CompanyEntity;

public interface CompanyRepository extends JpaRepository<CompanyEntity, String> {

}
